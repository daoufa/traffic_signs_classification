from django.http import HttpResponse
from django.shortcuts import render
# from .forms import GetImageToPredict
# from .models import RowImage
# Create your views here.

import pickle
import cv2
import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
from keras.models import load_model
from keras.preprocessing import image

# model
loaded_model = load_model(
    './static/resources/model-3x3-4Conv2D_dropout02__n14.h5')

# with open('./static/resources/data2.pickle', 'rb') as f:
#     data = pickle.load(f, encoding='latin1')  # dictionary type
# data['x_test'] = data['x_test'].transpose(0, 2, 3, 1)  # (12630, 32, 32, 3)


# Defining function for getting texts for every class - labels
def label_text(file):
    # Defining list for saving label in order from 0 to 42
    label_list = []

    # Reading 'csv' file and getting image's labels
    r = pd.read_csv(file)
    # Going through all names
    for name in r['SignName']:
        # Adding from every row second column with name of the label
        label_list.append(name)

    # Returning resulted list with labels
    return label_list


# Getting labels
labels = label_text('./static/resources/label_names.csv')


def home_view(request, *args, **kwargs):
    # form = GetImageToPredict()
    # img = RowImage.objects.all()
    # return render(request, "home.html", {"img": img, "form": form})
    return render(request, "home.html", {})


# def showImage(request, *args, **kwargs):
#     if request.method == "POST":
#         form = GetImageToPredict(data=request.POST, files=request.FILES)
#         if form.is_valid():
#             form.save()
#             obj = form.instance

#             img = image.load_img('.'+obj.image.url, target_size=(32, 32))
#             # img = request.FILES[0]
#             print("**************")
#             print(type(img))
#             img.resize((32, 32))
#             x = image.img_to_array(img)
#             x = np.expand_dims(x, axis=0)

#             images = np.vstack([x])
#             images = images / 255.0

#             scores = loaded_model.predict(images)
#             prediction = np.argmax(scores)
#             print('++predicted : ', labels[prediction])

#             context = {
#                 "obj": obj,
#                 "label": labels[prediction]
#             }

#             return render(request, "uploadImage.html", context)
#     else:
#         form = GetImageToPredict()
#         img = RowImage.objects.all()
#     return render(request, "home.html", {"img": img, "form": form})


def predictImage(request, *args, **kwargs):
    if request.method == "POST":
        img1 = cv2.imdecode(np.fromstring(
            request.FILES['image'].read(), np.uint8), 1)
        img = img1[..., ::-1].astype(np.float32)
        img = image.array_to_img(img, scale=True)

        img = img.resize((32, 32))
        x = image.img_to_array(img)

        x = np.expand_dims(x, axis=0)

        images = np.vstack([x])
        images = images / 255.0

        scores = loaded_model.predict(images)
        prediction = np.argmax(scores)
        print('++predicted : ', labels[prediction])

        # image_uri = to_data_uri(img)

        context = {
            "label": labels[prediction]
        }

        return render(request, "uploadImage.html", context)
        # form = GetImageToPredict()
        # img = RowImage.objects.all()
        # return render(request, "home.html", {"img": img, "form": form})
    return render(request, "home.html", {})
